===============================
libmfa
===============================


.. image:: https://img.shields.io/pypi/v/libmfa.svg
        :target: https://pypi.python.org/pypi/libmfa

.. image:: https://img.shields.io/travis/autoferrit/libmfa.svg
        :target: https://travis-ci.org/autoferrit/libmfa

.. image:: https://readthedocs.org/projects/libmfa/badge/?version=latest
        :target: https://libmfa.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/autoferrit/libmfa/shield.svg
     :target: https://pyup.io/repos/github/autoferrit/libmfa/
     :alt: Updates


The goal of this library, is to make it easier to generate tokens based on
a 6 digit pass key


* Free software: MIT license
* Documentation: https://libmfa.readthedocs.io.


Features
--------

* TODO
    Actual work

